﻿using Microsoft.Extensions.Configuration;

namespace EazySDK.Elements
{
    public class Other
    {
        private readonly IConfiguration _configuration;
        private int BankHolidayUpdateDays { get; set; }
        private bool ForceUpdateSchedulesOnRun { get; set; }

        public Other(IConfiguration config)
        {
            _configuration = config;
        }

        public int GetBankHolidayUpdateDays()
        {
            return int.MaxValue;
        }

        public bool GetForceUpdateSchedulesOnRun()
        {
            return false;
        }
    }
}

