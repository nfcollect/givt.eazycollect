﻿using Microsoft.Extensions.Configuration;

namespace EazySDK.Elements
{
    public class Contracts
    {
        private readonly IConfiguration _configuration;
        private bool AutoFixStartDate { get; set; }
        private bool AutoFixTerminationTypeAdHoc { get; set; }
        private bool AutoFixAtTheEndAdHoc { get; set; }
        private bool AutoFixPaymentDayInMonth { get; set; }
        private bool AutoFixPaymentMonthInYear { get; set; }

        public Contracts(IConfiguration config)
        {
            _configuration = config;
        }

        public bool GetAutoFixStartDate()
        {
            return false;
        }

        public bool GetAutoFixTerminationTypeAdHoc()
        {
            return false;
        }

        public bool GetAutoFixAtTheEndAdHoc()
        {
            return false;
        }

        public bool GetAutoFixPaymentDayInMonth()
        {
            return false;
        }


        public bool GetAutoFixPaymentMonthInYear()
        {
            {
                return false;
            }
        }

    }
}

