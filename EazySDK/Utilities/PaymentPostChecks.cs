﻿using System;
using Microsoft.Extensions.Configuration;

namespace EazySDK.Utilities
{
    public class PaymentPostChecks
    {
        /// <summary>
        /// Check that the collection_date argument is a valid ISO date and is at least x working days in the future, where x is the predetermined OngoingProcessingDays setting.
        /// Throw an error if this is not the case.
        /// </summary>
        /// 
        /// <param name="CollectionDate">A collection date provided by an external function</param>
        /// <param name="Settings">Settings configuration used for making a call to EazyCustomerManager</param>
        /// 
        /// <example>
        /// CheckPaymentDate("2019-06-24", Settings)
        /// </example>
        /// 
        /// <returns>
        /// date formatted as string
        /// </returns>
        public string CheckPaymentDate(string CollectionDate, IConfiguration Settings)
        {

            return CollectionDate;

        }

        /// <summary>
        /// Check whether or not is_credit is enabled for a client. This function does not check whether is_credit is enabled through EazyCustomerManager, 
        /// instead it checks the setting in AppSettings.json. Regardless of the setting, IsCredit will fail if it is  disallowed on EazyCustomerManager.
        /// </summary>
        /// 
        /// <param name="Settings">Settings configuration used for making a call to EazyCustomerManager</param>
        /// 
        /// <example>
        /// CheckIfCreditIsAllowed(false, Settings)
        /// </example>
        /// 
        /// <returns>
        /// bool
        /// </returns>
        public bool CheckIfCreditIsAllowed(IConfiguration Settings)
        {
            return true;
        }

        /// <summary>
        /// Check that the PaymentAmount is above 0.00. If this is not the case, throw an error.
        /// </summary>
        /// 
        /// <param name="PaymentAmount">A PaymentAmount provided by an external function</param>
        /// 
        /// <example>
        /// CheckPaymentAmount("123.45")
        /// </example>
        /// 
        /// <returns>
        /// bool
        /// </returns>
        public bool CheckPaymentAmount(string PaymentAmount)
        {
            float fPaymentAmount = new float();

            try
            {
                fPaymentAmount = float.Parse(PaymentAmount);
            }
            catch (FormatException)
            {
                throw new Exceptions.InvalidParameterException("The payment amount must be formatted as GBP currency, using x.xx formatting. " + PaymentAmount + " does not conform to this formatting.");
            }

            if (fPaymentAmount >= 0.01)
            {
                return true;
            }
            else
            {
                throw new Exceptions.InvalidParameterException("The payment amount must be at least £0.01. £" + PaymentAmount + " is too low.");
            }
        }
    }
}
